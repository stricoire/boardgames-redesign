# Boardgames Redesign

## Redesign of "Shadow Hunters" boardgame (June 2019)

I love board games and design. Thus, with a group of friends we decided to redesign an original board game in the universe of Star Wars. I worked on it during **70 hours**, doing photoshop design of **all the 90 cards** with my friends.  <br>
It was a very fun experience and I learned a lot of **useful skills in photoshop**.

It's only a personal project.

Respectively, <br>
Shadow Hunter original design, and our Shadow Hunter Version

<figure>
  <img src="Pictures/shadowHunter1.png" width="200">
  <img src="Pictures/shadowHunter2.png" width="200">
  <img src="Pictures/Jedi_Yoda.png" width="200">
  <img src="Pictures/Sith_Vador.png" width="200">
</figure>

<br>

Illustrations by <a href="https://www.deviantart.com/hoon">Hoon</a>, fans of star wars, and from the Shadow Hunter game


<br>

## Redesign of "Similo" boardgame (November 2020)

I loved Avatar the last airbender universe, so I decided as a gift, to recreate
a similo version of it. <br>
I worked on it during **26 hours**, doing photoshop design of **all the 42 cards**.  <br>
It was a relatively easy project and I'm quite proud of the result.

It's only a personal project.

Respectively, <br>
Similo original design with myth, and our version of Similo based on Avatar the last airbender universe

<figure>
  <img src="Pictures/Similo_Mythes.jpg" width="400">
  <img src="Pictures/aang2.png" width="200">
  <img src="Pictures/Zuko.png" width="200">
</figure>


Illustrations by <a href="https://asterein.tumblr.com/">Asterein</a>, and from the Similo myth game

